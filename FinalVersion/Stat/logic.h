#ifndef LOGIC_H
#define LOGIC_H

#include <string>
#include <vector>
#include <exception>
#include "analyzeparams.h"
#include "table.h"
#include "IProgress.h"

class RandomizerFunction;
class DistanceFunction;

class Logic
{
public:

    Logic(IProgress* progress);
    virtual ~Logic();

    void LoadFile(std::string path);
    void ExportResults(std::string path) const;
    Table* Analyze(const AnalyzeParams& params);
    void Abort();

   const Table& resultTable() const;
   const Table& originalTable() const;
   const Table& statsTable() const;
   const Table& distanceTable() const;

   static inline double normalize(double value, double mean, double deviation);

private:
    std::vector<ResultStat> collectStatistics(const Table& table, unsigned int maxWindowSize, unsigned int randomizations);

    Table* compare(const Table& table, unsigned int maxWindowSize);
    Table* resultStatsToTable(const std::vector<ResultStat>& stats);

    DistanceFunction*   createDistanceFunction(const AnalyzeParams& params) const;
    RandomizerFunction* createRandomizerFunction(const AnalyzeParams& params) const;


    bool                        m_aborted;
    AnalyzeParams               m_lastParams;
    Table*                      m_tableOriginal;
    Table*                      m_tableResult;
    Table*                      m_tableStats;
    Table*                      m_tableDistance;
    DistanceFunction*   m_distanceFunction;
    RandomizerFunction* m_randomizerFunction;
    IProgress*          m_progress;
};

#include <queue>
#include <mutex>

template<class T>
class SynchronizedQueue {
public:
    SynchronizedQueue()
      : m_elements_mutex()
      , m_elements() {}
    void push(T element)
    {
        std::lock_guard<std::mutex> lock(m_elements_mutex);
        m_elements.push(element);
    }
    bool pop(T& outElement)
    {
        std::lock_guard<std::mutex> lock(m_elements_mutex);
        if (m_elements.empty())
        {
            return false;
        }
        else
        {
            outElement = m_elements.front();
            m_elements.pop();
            return true;
        }
    }

private:
    std::mutex      m_elements_mutex;
    std::queue<T>   m_elements;
};

class LogicException : public std::exception
{
public:
    LogicException(const std::string& message)
        : m_message(message) {}
    virtual const char* what() const throw()
    {
        return m_message.c_str();
    }
private:
    std::string m_message;
};

#endif // LOGIC_H
