#include "randomizerfunction.h"
#include "table.h"
#include <types.h>
#include <stdlib.h>
#include <time.h>

using namespace std;

uint random(uint N)
{
    return rand() / (RAND_MAX / N + 1);
}

void randomize(uint* baseSeed)
{
    if (!baseSeed) {
        time_t t = time(nullptr);
        srand( t );
    } else {
        srand(*baseSeed);
    }

}

// -----------------------------------------------
string PlotRandomize::toString() const
{
    return "Plot randomization";
}

Table* PlotRandomize::randomize(const Table& table) const
{
    Table* shiftedTable = new Table(table);

    // Plots are the columns
    size_t width = table.width();
    for (uint col = 0; col < width; ++col) {
        uint otherCol = random(width);
        if (col != otherCol )
        {
            shiftedTable -> replaceColumns(col, otherCol);
        }
    }
    return shiftedTable;
}


// -----------------------------------------------
string RandomShift::toString() const
{
    return "Random shift of species";
}

Table* RandomShift::randomize(const Table& table) const
{
    Table* shiftedTable = new Table(table);

    size_t width = shiftedTable->width();
    for (uint row = 0; row < shiftedTable->height(); row++) {
        uint shift = random(width);
        for (uint col = 0; col < width; col++) { //plotId
            if (col + shift < width)
                shiftedTable->m_table[row][col + shift] = table.m_table[row][col];
            else
                shiftedTable->m_table[row][col + shift - width] = table.m_table[row][col];
        }
    }
    return shiftedTable;
}

// -----------------------------------------------
string NoneRandomize::toString() const
{
    return "None";
}

Table* NoneRandomize::randomize(const Table& table) const
{ // Just a copy
    return new Table(table);
}
