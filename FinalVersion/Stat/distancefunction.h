#ifndef DISTANCEFUNCTION_H
#define DISTANCEFUNCTION_H

#include <string>
#include "types.h"


/// Interface
class DistanceFunction
{
public:
    // Calculates the distance of the two vectors
    virtual Distance distance(const Vector& vector1, const Vector& vector2) const = 0;
    // Retuns the name of the functions
    virtual std::string toString() const = 0;
    virtual ~DistanceFunction() {}
};

/// Implementations

class EuclidianDistance : public DistanceFunction
{
public:
    virtual Distance distance(const Vector& vector1, const Vector& vector2) const;
    virtual std::string toString() const;
};

class EuclidianSquareDistance : public DistanceFunction
{
public:
    virtual Distance distance(const Vector& vector1, const Vector& vector2) const;
    virtual std::string toString() const;
};

class ManhattanDistance : public DistanceFunction
{
public:
    virtual Distance distance(const Vector& vector1, const Vector& vector2) const;
    virtual std::string toString() const;
};

class RenkonenDistance : public DistanceFunction
{
public:
    virtual Distance distance(const Vector& vector1, const Vector& vector2) const;
    virtual std::string toString() const;
};

class CzekanowskiDistance : public DistanceFunction
{
public:
    virtual Distance distance(const Vector& vector1, const Vector& vector2) const;
    virtual std::string toString() const;
};

#endif // DISTANCEFUNCTION_H
