#ifndef TABLE_H
#define TABLE_H

#include <vector>
#include <string>
#include <exception>
#include "types.h"
#include "distancefunction.h"

class Table
{
public:
    Table();
    Table(size_t height, size_t width);
    void LoadSelfFromFile(std::string filename);
    size_t  width() const;
    size_t height() const;
    const Vector&  row(uint index) const;
    E at(uint row, uint column) const;
    void set(uint row, uint column, E value);

    // Formats the table to a string
    std::string toString() const;
    // Formats to the format that is used at exporting
    std::string toExportString(bool printheader=true) const;

    Distance getDistance(uint offset, uint halfWindowSize, DistanceFunction* distFunction) const;
    void normalizeWithStat(const std::vector<ResultStat>& stat);

    Table* transpose() const;
    // Swaps two columns
    void replaceColumns(uint first, uint second);

private:
    typedef std::vector< std::vector<E> > table_t;

    size_t      m_width;
    size_t      m_height;
    table_t     m_table;

    friend class RandomShift;
};

class TableLoadException : public std::exception
{
public:
    TableLoadException(const std::string& message)
        : m_message(message) {}
    virtual const char* what() const throw()
    {
        return m_message.c_str();
    }
private:
    std::string m_message;
};

#endif // TABLE_H
