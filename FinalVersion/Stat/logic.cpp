#include "logic.h"

using namespace std;

#include <fstream>
#include <iostream>
#include <iomanip>
#include <math.h>
#include <climits> // For uintmax
#include <algorithm>

#include <thread>
#include "randomizerfunction.h"
#include "distancefunction.h"

Logic::Logic(IProgress* progress)
    : m_aborted(false)
    , m_lastParams()
    , m_tableOriginal(NULL)
    , m_tableResult(NULL)
    , m_tableStats(NULL)
    , m_tableDistance(NULL)
    , m_distanceFunction(NULL)
    , m_randomizerFunction(NULL)
    , m_progress(progress)
{
    randomize(nullptr);
}

Logic::~Logic()
{
    delete m_tableOriginal;
    delete m_tableResult;
    delete m_tableStats;
    delete m_tableDistance;
    delete m_distanceFunction;
    delete m_randomizerFunction;
}

void Logic::LoadFile(std::string path)
{
    if (m_tableOriginal) delete m_tableOriginal;
    Table* table = new Table();
    table->LoadSelfFromFile(path);
    m_tableOriginal = table->transpose();

    delete table;
}

void Logic::ExportResults(std::string path) const
{
    ofstream f(path.c_str()); // Open file

    f << "Moving split window analysis" << endl;
    f << "Maximum window size range: " << m_lastParams.maxHalfWindow() << endl;
    f << "Randomization count: " << m_lastParams.randomizationCount() << endl;

    if (m_tableOriginal)
    {
        f << "Row number:" << m_tableOriginal->height() << endl; // Table is transposed in the memory
        f << "Species number: " << m_tableOriginal->width() << endl;
    }

    if (m_distanceFunction)
    {
        f << "Comparative function: " << m_distanceFunction->toString() << endl;
    }
    if (m_randomizerFunction)
    {
        f << "Random reference with: " << m_randomizerFunction->toString() << endl;
    }
    f << "--------------------------------------"<< endl;
    if (m_tableStats)
    {
        f << "Statistics: "  << endl;
        //f << "    " << " | ";
        /*f << setprecision(5) << setw(10) << setfill(' ') << "mean";
        f << setprecision(5) << setw(10) << setfill(' ') << "deviation";
        f << setprecision(5) << setw(10) << setfill(' ') << "P10";
        f << setprecision(5) << setw(10) << setfill(' ') << "p05";
        f << setprecision(5) << setw(10) << setfill(' ') << "p025";
        f << setprecision(5) << setw(10) << setfill(' ') << "p01";
        f << endl;*/

        f << "window" << '\t';
        f << "mean" << '\t';
        f << "deviation" << '\t';
        f << "P10" << '\t';
        f << "p05" << '\t';
        f << "p025" << '\t';
        f << "p01" << '\t';
        f << endl;

        f << m_tableStats->toExportString(false) << endl << endl;
    }
    if (m_tableDistance)
    {
        f << "Distances: "  << endl;
        f << m_tableDistance->toExportString() << endl << endl;
    }
    if (m_tableResult)
    {
        f << "Normalized: "  << endl;
        f << m_tableResult->toExportString() << endl << endl;
    }
    f.close();
}

const Table& Logic::resultTable() const {
    return *m_tableResult;
}
const Table& Logic::originalTable() const
{
    return *m_tableOriginal;
}
const Table& Logic::statsTable() const
{
    return *m_tableStats;
}
const Table& Logic::distanceTable() const
{
    return *m_tableDistance;
}

inline double Logic::normalize(double value, double mean, double deviation)
{
    return (value - mean) / deviation;
}

vector<ResultStat> Logic::collectStatistics(const Table& table, unsigned int maxWindowSize, unsigned int randomizations)
{
    //Log::info("collectStatistics started");
    vector<ResultStat> resultStats(maxWindowSize);

    const size_t width = table.width();
    // Calculator function, this will be called by threads
    auto calculation = [&](uint randomSeed, int halfWindowSize) {
        randomize(&randomSeed);
        vector<Distance> topValues;
        double a = 0.0;
        double b = 0.0;
        double c = 0.0;
        for (unsigned int randomRun = 0; randomRun < randomizations; ++randomRun)
        {
            Table* randomTable = m_randomizerFunction->randomize(table);
            for (uint offset = 0; offset < (width - halfWindowSize * 2 + 1); ++offset) {
                Distance diff = randomTable->getDistance(offset, halfWindowSize, m_distanceFunction);
                a += diff;
                b += diff*diff;
                c += 1;

                topValues.push_back(diff);
                if (m_aborted) {
                    return;
                }
            }
            //m_progress->updateProgress(); If the update happens here it is more accurate, but very slow
            delete randomTable;
        }

        const uint indexP10  = c * (1.0 - 0.10); // top 90%
        const uint indexP05  = c * (1.0 - 0.05); // top 95%
        const uint indexP025 = c * (1.0 - 0.025);// top 97.5%
        const uint indexP01  = c * (1.0 - 0.01); // top 99%

        double mean, variance, deviation;
        mean = a / c; //mean
        variance = (b - (a * a / c)) / (c - 1); //variance
        deviation = sqrt(variance); //standard deviation

        std::sort(topValues.begin(), topValues.end());

        resultStats[halfWindowSize-1].mean = mean;
        resultStats[halfWindowSize-1].deviation = deviation;
        resultStats[halfWindowSize-1].p10  = normalize(topValues[indexP10], mean, deviation);
        resultStats[halfWindowSize-1].p05  = normalize(topValues[indexP05], mean, deviation);
        resultStats[halfWindowSize-1].p025 = normalize(topValues[indexP025], mean, deviation);
        resultStats[halfWindowSize-1].p01  = normalize(topValues[indexP01], mean, deviation);

        m_progress->updateProgress(randomizations);
    };

    // Create working threads
    uint processorCount = std::thread::hardware_concurrency();
    vector<thread*> threads(processorCount);

     // Prepare the task queue
    SynchronizedQueue<uint> workQueue;
    for (uint halfWindowSize = 1; halfWindowSize <= maxWindowSize; ++halfWindowSize)
    {
       workQueue.push(halfWindowSize);
       workQueue.push(random(UINT_MAX)); // In windows all threads have to be randomized. The main thread provides the seeds
    }

    // Start them
    for (uint i=0; i < processorCount; ++i)
    {
        thread* newThread = new std::thread([&](){
            uint nextItem;
            uint randomSeed;
            while (  workQueue.pop(nextItem) )
            {
                workQueue.pop(randomSeed);
                calculation(randomSeed, nextItem);
                if (m_aborted)
                {
                    break;
                }
            }
        });
        threads[i] = newThread;
    }

    // Wait them to finish
    for (thread* t : threads)
    {
        t->join();
        delete t;
    }

    return resultStats;
}

Table* Logic::compare(const Table& table, unsigned int maxWindowSize)
{
    size_t width = table.width();
    size_t maxOffset = width - 2;
    Table* result = new Table(maxOffset+1, maxWindowSize);

    for (unsigned int halfWindowSize = 1; halfWindowSize <= maxWindowSize; ++halfWindowSize)    // Check for all window sizes
    {
        for (uint offset = 0; offset < (width - halfWindowSize * 2 + 1); offset++) // Window sliding
        {
            Distance diff = table.getDistance(offset, halfWindowSize, m_distanceFunction);
            result->set(offset+ (halfWindowSize-1/*for the triangle like shape*/) , halfWindowSize-1, diff);
            if (m_aborted) return result;
        }
    }
    m_progress->updateProgress(maxWindowSize);

    return result;
}


Table* Logic::Analyze(const AnalyzeParams& params)
{
    m_aborted = false;
    m_lastParams = params;
    m_distanceFunction   = createDistanceFunction(params);
    m_randomizerFunction = createRandomizerFunction(params);

    uint maxHalfWindow = std::min((size_t)params.maxHalfWindow(), m_tableOriginal->width()/2 );

    m_progress->setAllCalculations(maxHalfWindow * (params.randomizationCount()+1) );

    // Random statistics
    vector<ResultStat> stats = collectStatistics(*m_tableOriginal, maxHalfWindow, params.randomizationCount());
    if (m_aborted) return nullptr;

    // Compare
    if (m_tableDistance) delete m_tableDistance;
    m_tableDistance = compare(*m_tableOriginal, maxHalfWindow);

    if (m_tableStats) delete m_tableStats;
    m_tableStats = resultStatsToTable(stats);

    // Normalize
    if (m_tableResult) delete m_tableResult;
    m_tableResult = new Table(*m_tableDistance);
    m_tableResult -> normalizeWithStat(stats);

    m_progress -> finish();

    return m_tableResult;
}

void Logic::Abort()
{
    m_aborted = true;
}

Table* Logic::resultStatsToTable(const vector<ResultStat>& stats) {
    Table* result = new Table(stats.size(), 6);
    for (uint i = 0; i < stats.size(); ++i)
    {
        uint j = 0;
        //result->set(i, j++, i+1); // Index
        result->set(i, j++, stats[i].mean);
        result->set(i, j++, stats[i].deviation);
        result->set(i, j++, stats[i].p10);
        result->set(i, j++, stats[i].p05);
        result->set(i, j++, stats[i].p025);
        result->set(i, j++, stats[i].p01);
    }
    return result;
}

DistanceFunction* Logic::createDistanceFunction(const AnalyzeParams& params) const
{
    switch (params.distance())
    {
    case AnalyzeParams::EUCLIDIAN:
        return new EuclidianDistance();
    case AnalyzeParams::EUCLIDIAN_SQUARE:
        return new EuclidianSquareDistance();
    case AnalyzeParams::MANHATTAN:
        return new ManhattanDistance();
    case AnalyzeParams::RENKONEN_INDEX_COMPLEMENTER:
        return new RenkonenDistance();
    case AnalyzeParams::CZEKANOWSKI:
        return new CzekanowskiDistance();
    default:
        return new EuclidianDistance();
    }
}

RandomizerFunction* Logic::createRandomizerFunction(const AnalyzeParams& params) const
{
    switch (params.randomization())
    {
    case AnalyzeParams::PLOT_RANDOMIZATION:
        return new PlotRandomize();
    case AnalyzeParams::RANDOM_SHIFT:
        return new RandomShift();
    case AnalyzeParams::NONE:
        return new NoneRandomize();
    default:
        return new NoneRandomize();
    }
}
