#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QThread>
#include "table.h"
#include "IProgress.h"
#include "analyzeparams.h"

class Logic;

namespace Ui {
class MainWindow;
}

class WorkerThread;

class MainWindow : public QMainWindow
{
    Q_OBJECT
    friend class WorkerThread;
public:
    enum UIState {
        EMPTY,
        LOADED,
        ANALYZING,
        ANALYZED
    };

    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    static const char* SETTINGS_FILENAME;
    static const char* SETTINGS_KEY_MAXWINDOW;
    static const char* SETTINGS_KEY_RANDOMIZATIONS;

    Ui::MainWindow *ui;
    Logic* logic;
    UIState currentState = EMPTY;
    WorkerThread* workerThread;

    void loadSettings();
    void saveSettings(AnalyzeParams params);

    AnalyzeParams getAnalyzeParamsFromUI();
    void UpdateState(UIState state);
    void exportData();
    void fillData(const Table& table, bool statistics = false);
    QStringList getHeaderList(bool statisticsList);

    void enableResultButtons(bool enable);
    void enableDistanceButtons(bool enable);
    void enableRandomizationButtons(bool enable);

    void createWorkerThread();

public slots:
    void on_calculation_finished(Table* resultTable);
    void on_calculation_error(std::string errorMessage);
    void on_progress_updated(int value);

private slots:
    void on_actionOpen_triggered();
    void on_analyzeButton_clicked();
    void on_actionSave_triggered();
    void on_RESULT_1_STATISTICS_toggled(bool checked);
    void on_RESULT_2_DISTANCES_toggled(bool checked);
    void on_RESULT_3_NORMALIZED_toggled(bool checked);
    void on_RESULT_4_ORIGINAL_toggled(bool checked);
    void on_abortButton_clicked();
    void on_actionExit_triggered();
};

class WorkerThread : public QThread, public IProgress
{
    Q_OBJECT
public:
    WorkerThread() : m_logic(nullptr), m_params(){}
    void setParams(AnalyzeParams params) {m_params = params;}
    void Init(Logic* logic);

signals:
    void analyzeFinished(Table* resultTable);
    void analyzeError(std::string msg);
    void progressUpdated(int value);

protected:
    // IProgress
    virtual void onProgressUpdate();
    // QThread
    virtual void run();

private:
    Table* doAnalyze();

    Logic* m_logic;
    AnalyzeParams m_params;
};

#endif // MAINWINDOW_H
