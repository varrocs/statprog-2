#include <QFileDialog>
#include <QStandardItemModel>
#include <QMessageBox>
#include <QSettings>
#include <iostream>
//#include <thread>
#include <future>

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "logic.h"
#include "table.h"
#include "IProgress.h"

using namespace std;

const char* MainWindow::SETTINGS_FILENAME = "settings.ini";
const char* MainWindow::SETTINGS_KEY_MAXWINDOW = "max_window_width";
const char* MainWindow::SETTINGS_KEY_RANDOMIZATIONS = "randomizations";

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    logic(NULL),
    currentState(EMPTY),
    workerThread(NULL)
{
    // Create stuff
    createWorkerThread();
    logic = new Logic(workerThread);
    workerThread->Init(logic);

    // Initialize stuff
    ui->setupUi(this);
    loadSettings();
    UpdateState(UIState::EMPTY);
}

MainWindow::~MainWindow()
{
    delete ui;
    delete logic;
}

void MainWindow::loadSettings()
{
    try {
        QSettings settings(SETTINGS_FILENAME, QSettings::IniFormat);
        int randomizations = settings.value(SETTINGS_KEY_RANDOMIZATIONS, 1000).toInt(NULL);
        int maxWindowWidth = settings.value(SETTINGS_KEY_MAXWINDOW, 25).toInt(NULL);

        ui->SPIN_HALF_WIN->setValue(maxWindowWidth);
        ui->SPIN_RANDOMIZATIONS->setValue(randomizations);
    } catch (...) {
        cout << "Failed to load settings" << endl;
    }
}

void MainWindow::saveSettings(AnalyzeParams params)
{
    try {
        QSettings settings(SETTINGS_FILENAME, QSettings::IniFormat);
        settings.setValue(SETTINGS_KEY_MAXWINDOW, params.maxHalfWindow());
        settings.setValue(SETTINGS_KEY_RANDOMIZATIONS, params.randomizationCount());
    } catch (...) {
        cout << "Failed to save settings" << endl;
    }
}

void MainWindow::on_actionOpen_triggered()
{
    QString openFileName = QFileDialog::getOpenFileName(this, "Open a file");
    if (openFileName.isNull())
    { // Cancel
        return;
    }

    try {
        string nativeText = openFileName.toLocal8Bit().constData();
        logic->LoadFile(nativeText);
        fillData( logic->originalTable() );
        UpdateState(UIState::LOADED);
    }
    catch (const TableLoadException& e)
    {
        QMessageBox messagebox;
        messagebox.setText(e.what());
        messagebox.show();
        messagebox.exec();
    }
}

void MainWindow::createWorkerThread()
{
    if (workerThread) delete workerThread;
    workerThread = new WorkerThread();

    QObject::connect(workerThread, SIGNAL(analyzeFinished(Table*)),    this, SLOT(on_calculation_finished(Table*)) );
    QObject::connect(workerThread, SIGNAL(analyzeError(std::string)), this, SLOT(on_calculation_error(std::string)) );
    QObject::connect(workerThread, SIGNAL(progressUpdated(int)), this, SLOT(on_progress_updated(int)) );
}

void MainWindow::on_analyzeButton_clicked()
{
    UpdateState(UIState::ANALYZING);
    AnalyzeParams params = getAnalyzeParamsFromUI();
    saveSettings(params);
    workerThread->setParams(params);
    workerThread->start();
}

void MainWindow::on_calculation_finished(Table* result)
{
    if (result) {
        UpdateState(UIState::ANALYZED);
        ui->RESULT_3_NORMALIZED->click();
        fillData(*result);
    } else {
        UpdateState(UIState::LOADED);
    }
}

void MainWindow::on_calculation_error(std::string errorMessage)
{
    QMessageBox messageBox;
    messageBox.setText(errorMessage.c_str());
    messageBox.exec();
}

void MainWindow::on_progress_updated(int value)
{
    ui->progressBar->setValue(value);
}

AnalyzeParams MainWindow::getAnalyzeParamsFromUI()
{
    AnalyzeParams::Distance distance;
         if (ui->DIST_1_EUCL->isChecked()) distance = AnalyzeParams::EUCLIDIAN;
    else if (ui->DIST_2_SQ_EUCL->isChecked()) distance = AnalyzeParams::EUCLIDIAN_SQUARE;
    else if (ui->DIST_3_MANH->isChecked()) distance = AnalyzeParams::MANHATTAN;
    else if (ui->DIST_4_RENKONEN->isChecked()) distance = AnalyzeParams::RENKONEN_INDEX_COMPLEMENTER;
    else if (ui->DIST_5_DZ->isChecked()) distance = AnalyzeParams::CZEKANOWSKI;
    else distance = AnalyzeParams::EUCLIDIAN;

    AnalyzeParams::Randomization randomization;
         if (ui->RAND_1_SHIFT->isChecked()) randomization = AnalyzeParams::RANDOM_SHIFT;
    else if (ui->RAND_2_PLOT->isChecked()) randomization = AnalyzeParams::PLOT_RANDOMIZATION;
    else randomization = AnalyzeParams::RANDOM_SHIFT;

    int maxWindow = ui->SPIN_HALF_WIN->value();
    int randomRun = ui->SPIN_RANDOMIZATIONS->value();

    AnalyzeParams result(distance, randomization, randomRun, maxWindow );
    return result;
}

QStringList MainWindow::getHeaderList(bool statisticsList) {
    QStringList headerList;
    if (statisticsList)
    {
        headerList.append("Mean");
        headerList.append("Deviation");

        headerList.append("P10");
        headerList.append("P05");
        headerList.append("P025");
        headerList.append("P01");
    }
    else
    {
        headerList.append("1");
        headerList.append("2");
        headerList.append("3");
        headerList.append("4");
        headerList.append("5");
        headerList.append("6");
    }
    return headerList;
}

void MainWindow::fillData(const Table& table, bool statistics)
{
    QStandardItemModel* model = new QStandardItemModel(table.height(), table.width(), this);

    model->setHorizontalHeaderLabels(getHeaderList(statistics));

    for (uint row = 0; row<table.height(); ++row)
    {
        for (uint column = 0; column < table.width(); ++column)
        {
            model->setItem(row, column, new QStandardItem());
            model->item(row, column)->setText( QString::number( table.at(row, column)) );
        }
    }
    ui->tableView->setModel(model);
    ui->tableView->setVisible(false);   // Set the columns to the proper size
    ui->tableView->resizeColumnsToContents();
    ui->tableView->setVisible(true);
    //ui->tableView->resizeColumnsToContents();
}

void MainWindow::UpdateState(UIState state)
{
    currentState = state;
    switch (currentState) {
    case EMPTY:
        enableResultButtons(false);
        enableDistanceButtons(false);
        enableRandomizationButtons(false);
        ui->analyzeButton->setEnabled(false);
        ui->abortButton->setEnabled(false);
        break;
    case LOADED:
        enableResultButtons(false);
        enableDistanceButtons(true);
        enableRandomizationButtons(true);
        ui->analyzeButton->setEnabled(true);
        ui->abortButton->setEnabled(false);

        ui->progressBar->setMaximum(0);
        ui->progressBar->setMaximum(100);
        ui->progressBar->setValue(0);
        break;
    case ANALYZING:
        enableResultButtons(false);
        enableDistanceButtons(false);
        enableRandomizationButtons(false);
        ui->analyzeButton->setEnabled(false);
        ui->abortButton->setEnabled(true);

        ui->progressBar->setMaximum(0);
        ui->progressBar->setMaximum(100);
        ui->progressBar->setValue(0);
        break;
    case ANALYZED:
        enableResultButtons(true);
        enableResultButtons(true);
        enableDistanceButtons(true);
        enableRandomizationButtons(true);
        ui->analyzeButton->setEnabled(true);
        ui->abortButton->setEnabled(false);
        break;

    default:
        break;
    }
}

void MainWindow::on_actionSave_triggered()
{
    QString exportFileName = QFileDialog::getSaveFileName(this,"Export file");
    string nativeText = exportFileName.toLocal8Bit().constData();   // Convert the text to local format(Windows needs this)
    logic->ExportResults(nativeText);
}

void MainWindow::on_RESULT_1_STATISTICS_toggled(bool checked)
{
    if (checked)
    {
        fillData(logic->statsTable(), true);
    }
}

void MainWindow::on_RESULT_2_DISTANCES_toggled(bool checked)
{
    if (checked)
    {
        fillData(logic->distanceTable());
    }
}

void MainWindow::on_RESULT_3_NORMALIZED_toggled(bool checked)
{
    if (checked)
    {
        fillData(logic->resultTable());
    }
}

void MainWindow::on_RESULT_4_ORIGINAL_toggled(bool checked)
{
    if (checked)
    {
        fillData(logic->originalTable());
    }
}

void MainWindow::enableResultButtons(bool enable)
{
    ui->RESULT_1_STATISTICS->setEnabled(enable);
    ui->RESULT_2_DISTANCES->setEnabled(enable);
    ui->RESULT_3_NORMALIZED->setEnabled(enable);
    ui->RESULT_4_ORIGINAL->setEnabled(enable);
}

void MainWindow::enableDistanceButtons(bool enable)
{
    ui->DIST_1_EUCL->setEnabled(enable);
    ui->DIST_2_SQ_EUCL->setEnabled(enable);
    ui->DIST_3_MANH->setEnabled(enable);
    ui->DIST_4_RENKONEN->setEnabled(enable);
    ui->DIST_5_DZ->setEnabled(enable);
}

void MainWindow::enableRandomizationButtons(bool enable)
{
    ui->RAND_1_SHIFT->setEnabled(enable);
    ui->RAND_2_PLOT->setEnabled(enable);
}

void MainWindow::on_abortButton_clicked()
{
    if (logic) logic->Abort();
    UpdateState(UIState::LOADED);
}

void MainWindow::on_actionExit_triggered()
{
    exit(1);
}

// ----------------------------------------------------------------------------

void WorkerThread::Init(Logic* logic)
{
    this->m_logic = logic;
}

void WorkerThread::onProgressUpdate()
{
   emit progressUpdated(  (int)(percentage()*100.0) );
}

void WorkerThread::run()
{
    Table* result = doAnalyze();
    emit analyzeFinished(result);
}

Table* WorkerThread::doAnalyze()
{
    if ( ! m_logic)
    {
        cerr << "Worker thread is not initialized" << endl;
    }
    Table* result = nullptr;
    try
    {
        result = m_logic->Analyze(m_params);
    }
    catch (const LogicException& e)
    {
        emit analyzeError(e.what());
        return nullptr;
    }

    return result;
}
