#ifndef IPROGRESS_H
#define IPROGRESS_H

#include <mutex>

// Abstract parent class, for progress callback listeners
class IProgress
{
public:
    IProgress()
        : m_allCalculations(0)
        , m_calculations(0)
    {}

    virtual ~IProgress(){}

    void updateProgress(long progress = 1)
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        m_calculations += progress;
        onProgressUpdate();
    }
    void setAllCalculations(long allOperationsCount)
    {
        m_allCalculations = allOperationsCount;
        m_calculations = 0;
    }

    void finish()
    {
        updateProgress(m_allCalculations);
    }

protected:

    double percentage() {return (double)m_calculations / (double)m_allCalculations;}

    long m_allCalculations;
    long m_calculations;
private:
    std::mutex m_mutex;

    // This has to be overwritten by the derivated classes
    virtual void onProgressUpdate() = 0;
};

// Dummy implementation
class ConsoleProgress :  public IProgress {
    virtual void onProgressUpdate();
};

#endif // IPROGRESS_H
