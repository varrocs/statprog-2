#include "mainwindow.h"
#include <iostream>
#include <QApplication>
#include "table.h"
#include "analyzeparams.h"
#include "logic.h"
#include "IProgress.h"

using namespace std;

#ifdef CLI
void printUsage(char* binaryName) {
    cout << "Usage: " << binaryName <<" <CSV path> <Distance function> <random shift type> <randomization count> <max half window size>" << endl;
    cout << "Distance functions: " << endl;
    cout << "EUCLIDIAN" << endl;
    cout << "SQUARED_EUCLIDIAN" << endl;
    cout << "MANHATTAM" << endl;
    cout << "RENKONEN" << endl;
    cout << "CZEKANOWSKI" << endl;
    cout << endl;

    cout << "Randomization: " << endl;
    cout << "RANDOM_SHIFT" << endl;
    cout << "PLOT_RANDOMIZATION" << endl;
    cout << "NONE" << endl;
    cout << endl;

}

AnalyzeParams* readParams(int argc, char** argv) {
    if (argc < 4)
    {
        cerr <<  "Not enough arguments" << endl;
        return nullptr;
    }
    AnalyzeParams::Distance distance;
    AnalyzeParams::Randomization randomization;
    int randomizationCount;
    int maxHalfWindow;
    int paramcounter = 0;

    // Distance
    string distanceStr = argv[paramcounter++];
    if (distanceStr == "EUCLIDIAN") distance = AnalyzeParams::EUCLIDIAN;
    else if (distanceStr == "EUCLIDIAN_SQUARE") distance = AnalyzeParams::EUCLIDIAN_SQUARE;
    else if (distanceStr == "MANHATTAN") distance = AnalyzeParams::MANHATTAN;
    else if (distanceStr == "RENKONEN") distance = AnalyzeParams::RENKONEN_INDEX_COMPLEMENTER;
    else if (distanceStr == "CZEKANOWSKI") distance = AnalyzeParams::CZEKANOWSKI;
    else
    {
        cerr << "Unrecognized distance function: " << distanceStr;
        return nullptr;
    }

    // Rand
    string randomizationStr = argv[paramcounter++];
    if (randomizationStr == "RANDOM_SHIFT") randomization = AnalyzeParams::RANDOM_SHIFT;
    else if (randomizationStr == "PLOT_RANDOMIZATION") randomization = AnalyzeParams::PLOT_RANDOMIZATION;
    else if (randomizationStr == "NONE") randomization = AnalyzeParams::NONE;
    else
    {
        cerr << "Unrecognized randomization: " << randomizationStr;
        return nullptr;
    }

    randomizationCount = atoi(argv[paramcounter++]);
    maxHalfWindow = atoi(argv[paramcounter++]);

    return new AnalyzeParams(distance, randomization, randomizationCount, maxHalfWindow);

}

int climain(int argc, char *argv[])
{
   IProgress* progress = new ConsoleProgress();

   Logic l(progress);    // This object does the calculation

   std::string path;
   AnalyzeParams* params;
   if (argc < 6 )
   {
       cerr << "Too few parameters("<<argc<<")" << endl;
       printUsage(argv[0]);
       exit(1);
   }
   else if (  (params = readParams(argc-2, argv+2)) == nullptr )
   {
       printUsage(argv[0]);
       exit(1);
   } else {
       path = argv[1];
   }

   try {
       l.LoadFile(path);
       string outPath = path + ".out.txt";
       Table* result = l.Analyze(*params);
       if (result) {
        l.ExportResults(outPath);
       }
       else
       {
           cerr << "Something was wrong with the randomization" << endl;
       }
   }
   catch (const exception& e)
   {
       cerr << "Problem while running: " << endl << e.what()  <<endl;
   }

   delete params;
   delete progress;

   return 0;
}
#endif

int main(int argc, char *argv[])
{
#ifdef CLI
    return climain(argc, argv);
#else
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    
    return a.exec();
#endif
}
