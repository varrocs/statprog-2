#include "distancefunction.h"
#include "table.h"
#include "math.h"

using namespace std;

//----------------------------------------------

string EuclidianDistance::toString() const
{
    return "Euclidian distance";
}

Distance EuclidianDistance::distance(const Vector& leftValues, const Vector& rightValues) const
{
    size_t size = leftValues.size();
    Distance sum = 0.0;
    for (size_t i = 0; i < size; ++i) {
        double diff = leftValues[i] - rightValues[i];
        sum += (diff*diff);
    }

    return sqrt(sum);
}

//----------------------------------------------

string EuclidianSquareDistance::toString() const
{
    return "Euclidian square distance";
}


Distance EuclidianSquareDistance::distance(const Vector& leftValues, const Vector& rightValues) const
{

    size_t size = leftValues.size();
    Distance sum = 0.0;
    for (size_t i = 0; i < size; ++i) {
        double diff = leftValues[i] - rightValues[i];
        sum += (diff*diff);
    }

    return sum;
}


//----------------------------------------------


string ManhattanDistance::toString() const
{
    return "Manhattan distance";
}


Distance ManhattanDistance::distance(const Vector& leftValues, const Vector& rightValues) const
{

    size_t size = leftValues.size();
    Distance sum = 0.0;
    for (size_t i = 0; i < size; ++i) {
        double diff = leftValues[i] - rightValues[i];
        sum += fabs(diff);
    }

    return sum;
}


//----------------------------------------------


string RenkonenDistance::toString() const
{
    return "Renkonen index";
}


Distance RenkonenDistance::distance(const Vector& leftValues, const Vector& rightValues) const
{

    size_t size = leftValues.size();
    Distance sum = 0.0;
    double rightSum = 0.0;
    double  leftSum = 0.0;

    // Calculate sums
    for (size_t i = 0; i < size; ++i) {
        leftSum += leftValues[i];
       rightSum += rightValues[i];
    }

    // Calculate the distance
    for (size_t i = 0; i < size; ++i) {

        sum += std::min(leftValues[i]/leftSum, rightValues[i]/rightSum);
    }

    return 1.0 - sum;
}


//----------------------------------------------


string CzekanowskiDistance::toString() const
{
    return "Czekanowski distance";
}


Distance CzekanowskiDistance::distance(const Vector& leftValues, const Vector& rightValues) const
{

    size_t size = leftValues.size();
    double leftSum = 0.0;
    double rightSum = 0.0;
    double commonSum = 0.0;

    for (size_t i = 0; i < size; ++i) {
         leftSum += leftValues[i];
        rightSum += rightValues[i];
        commonSum += std::min(rightValues[i],  leftValues[i]);
    }

    return 1.0 - 2*commonSum / (rightSum + leftSum);
}
