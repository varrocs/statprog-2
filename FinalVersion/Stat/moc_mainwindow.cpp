/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.0.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.0.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[20];
    char stringdata[377];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    offsetof(qt_meta_stringdata_MainWindow_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10),
QT_MOC_LITERAL(1, 11, 23),
QT_MOC_LITERAL(2, 35, 0),
QT_MOC_LITERAL(3, 36, 6),
QT_MOC_LITERAL(4, 43, 11),
QT_MOC_LITERAL(5, 55, 20),
QT_MOC_LITERAL(6, 76, 11),
QT_MOC_LITERAL(7, 88, 12),
QT_MOC_LITERAL(8, 101, 19),
QT_MOC_LITERAL(9, 121, 5),
QT_MOC_LITERAL(10, 127, 23),
QT_MOC_LITERAL(11, 151, 24),
QT_MOC_LITERAL(12, 176, 23),
QT_MOC_LITERAL(13, 200, 30),
QT_MOC_LITERAL(14, 231, 7),
QT_MOC_LITERAL(15, 239, 29),
QT_MOC_LITERAL(16, 269, 30),
QT_MOC_LITERAL(17, 300, 28),
QT_MOC_LITERAL(18, 329, 22),
QT_MOC_LITERAL(19, 352, 23)
    },
    "MainWindow\0on_calculation_finished\0\0"
    "Table*\0resultTable\0on_calculation_error\0"
    "std::string\0errorMessage\0on_progress_updated\0"
    "value\0on_actionOpen_triggered\0"
    "on_analyzeButton_clicked\0"
    "on_actionSave_triggered\0"
    "on_RESULT_1_STATISTICS_toggled\0checked\0"
    "on_RESULT_2_DISTANCES_toggled\0"
    "on_RESULT_3_NORMALIZED_toggled\0"
    "on_RESULT_4_ORIGINAL_toggled\0"
    "on_abortButton_clicked\0on_actionExit_triggered\0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      12,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   74,    2, 0x0a,
       5,    1,   77,    2, 0x0a,
       8,    1,   80,    2, 0x0a,
      10,    0,   83,    2, 0x08,
      11,    0,   84,    2, 0x08,
      12,    0,   85,    2, 0x08,
      13,    1,   86,    2, 0x08,
      15,    1,   89,    2, 0x08,
      16,    1,   92,    2, 0x08,
      17,    1,   95,    2, 0x08,
      18,    0,   98,    2, 0x08,
      19,    0,   99,    2, 0x08,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 6,    7,
    QMetaType::Void, QMetaType::Int,    9,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   14,
    QMetaType::Void, QMetaType::Bool,   14,
    QMetaType::Void, QMetaType::Bool,   14,
    QMetaType::Void, QMetaType::Bool,   14,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWindow *_t = static_cast<MainWindow *>(_o);
        switch (_id) {
        case 0: _t->on_calculation_finished((*reinterpret_cast< Table*(*)>(_a[1]))); break;
        case 1: _t->on_calculation_error((*reinterpret_cast< std::string(*)>(_a[1]))); break;
        case 2: _t->on_progress_updated((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->on_actionOpen_triggered(); break;
        case 4: _t->on_analyzeButton_clicked(); break;
        case 5: _t->on_actionSave_triggered(); break;
        case 6: _t->on_RESULT_1_STATISTICS_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 7: _t->on_RESULT_2_DISTANCES_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 8: _t->on_RESULT_3_NORMALIZED_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 9: _t->on_RESULT_4_ORIGINAL_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 10: _t->on_abortButton_clicked(); break;
        case 11: _t->on_actionExit_triggered(); break;
        default: ;
        }
    }
}

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow.data,
      qt_meta_data_MainWindow,  qt_static_metacall, 0, 0}
};


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 12)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 12;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 12)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 12;
    }
    return _id;
}
struct qt_meta_stringdata_WorkerThread_t {
    QByteArrayData data[10];
    char stringdata[101];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    offsetof(qt_meta_stringdata_WorkerThread_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData) \
    )
static const qt_meta_stringdata_WorkerThread_t qt_meta_stringdata_WorkerThread = {
    {
QT_MOC_LITERAL(0, 0, 12),
QT_MOC_LITERAL(1, 13, 15),
QT_MOC_LITERAL(2, 29, 0),
QT_MOC_LITERAL(3, 30, 6),
QT_MOC_LITERAL(4, 37, 11),
QT_MOC_LITERAL(5, 49, 12),
QT_MOC_LITERAL(6, 62, 11),
QT_MOC_LITERAL(7, 74, 3),
QT_MOC_LITERAL(8, 78, 15),
QT_MOC_LITERAL(9, 94, 5)
    },
    "WorkerThread\0analyzeFinished\0\0Table*\0"
    "resultTable\0analyzeError\0std::string\0"
    "msg\0progressUpdated\0value\0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_WorkerThread[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   29,    2, 0x05,
       5,    1,   32,    2, 0x05,
       8,    1,   35,    2, 0x05,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 6,    7,
    QMetaType::Void, QMetaType::Int,    9,

       0        // eod
};

void WorkerThread::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        WorkerThread *_t = static_cast<WorkerThread *>(_o);
        switch (_id) {
        case 0: _t->analyzeFinished((*reinterpret_cast< Table*(*)>(_a[1]))); break;
        case 1: _t->analyzeError((*reinterpret_cast< std::string(*)>(_a[1]))); break;
        case 2: _t->progressUpdated((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (WorkerThread::*_t)(Table * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&WorkerThread::analyzeFinished)) {
                *result = 0;
            }
        }
        {
            typedef void (WorkerThread::*_t)(std::string );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&WorkerThread::analyzeError)) {
                *result = 1;
            }
        }
        {
            typedef void (WorkerThread::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&WorkerThread::progressUpdated)) {
                *result = 2;
            }
        }
    }
}

const QMetaObject WorkerThread::staticMetaObject = {
    { &QThread::staticMetaObject, qt_meta_stringdata_WorkerThread.data,
      qt_meta_data_WorkerThread,  qt_static_metacall, 0, 0}
};


const QMetaObject *WorkerThread::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *WorkerThread::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_WorkerThread.stringdata))
        return static_cast<void*>(const_cast< WorkerThread*>(this));
    if (!strcmp(_clname, "IProgress"))
        return static_cast< IProgress*>(const_cast< WorkerThread*>(this));
    return QThread::qt_metacast(_clname);
}

int WorkerThread::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QThread::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 3)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 3;
    }
    return _id;
}

// SIGNAL 0
void WorkerThread::analyzeFinished(Table * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void WorkerThread::analyzeError(std::string _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void WorkerThread::progressUpdated(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_END_MOC_NAMESPACE
