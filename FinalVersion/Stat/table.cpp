#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>

#include "table.h"

using namespace std;

Table::Table()
    : m_width(0)
    , m_height(0)
    , m_table()
{
}

Table::Table(size_t height, size_t width)
    : m_width(width)
    , m_height(height)
    , m_table(height)
{
    for (uint i = 0; i < height; ++i) {
        m_table[i] = Vector(width);
    }
}

size_t Table::width() const
{
    return m_width;
}

size_t Table::height() const
{
    return m_height;
}

const Vector& Table::row(uint index) const
{
    return m_table[index];
}

E Table::at(uint row, uint column) const
{
    return m_table[row][column];
}

void Table::set(uint row, uint column, E value)
{
    m_table[row][column] = value;
}

void Table::LoadSelfFromFile(std::string filename)
{
    m_table.clear();

    // Read the file
    int linecounter = 0;

    ifstream infile(filename.c_str());
    string line;
    while (getline(infile, line))
    {
        if (line.empty() || line[0] == '#') continue; // Pass empty lines and comments
        m_table.push_back( vector<E>() ); // Adding a new line

        // Find the delimiter character
        char delimiter;
        if (line.find(',') != string::npos ) delimiter = ',';
        else if (line.find('\t') != string::npos ) delimiter = '\t';
        else if (line.find(';') != string::npos ) delimiter = ';';
        else delimiter = ' ';

        istringstream linestream(line);
        string cell;
        double number;
        unsigned int widthCounter = 0;

        while (getline(linestream, cell, delimiter))
        {
            istringstream cellstream(cell);
            cellstream >> number;
            m_table[linecounter].push_back(number);
            widthCounter++;
        }

        if (m_width != 0 && m_width != widthCounter)
        {   // Error handling
            stringstream str;
            str << "Not matching fields in line" << endl;
            str << "Expected: " << m_width << ", actual: " << widthCounter << endl;
            str << "In file " << filename << ", line number: " << linecounter+1 << endl;
            throw TableLoadException(str.str());
        }

        m_width = widthCounter;
        linecounter++;
    }

    m_height = linecounter;
}

string Table::toString() const
{
    stringstream result;
    for (uint row = 0; row < height(); row++)
    {
        for (uint col = 0; col < width(); col++)
        {
            result << at(row, col) << ",\t";
        }
        result << endl;
    }
    return result.str();
}

string Table::toExportString(bool printHeader) const
{
    stringstream result;
    // Header
    if (printHeader)
    {
        result << "index" << '\t';
        for (uint col = 0; col < width(); col++)
        {
            //result << setprecision(5) << setw(10) << setfill(' ') << (col+1);
            result << (col+1) << '\t';
        }
        result << endl;
    }
    // Table
    for (uint row = 0; row < height(); row++)
    {
        //result << setw(4) << setfill(' ') << (row+1) << " | "; // Row header
        result << (row+1) << '\t';
        for (uint col = 0; col < width(); col++)
        {
            //result << setprecision(5) << setw(10) << setfill(' ') << at(row, col);
            result << at(row, col) << '\t';
        }
        result << endl;
    }
    return result.str();
}

Distance Table::getDistance(uint offset, uint halfWindowSize, DistanceFunction* distanceFunction) const
{
    Vector leftAverages(m_height);
    Vector rightAverages(m_height);

    for (uint row = 0; row < m_height; ++row)
    {
        E leftAverage = 0.0;
        E rightAverage = 0.0;
        for (uint inWindow = 0; inWindow < halfWindowSize; ++inWindow) {
            leftAverage  += m_table[row][offset + inWindow];
            rightAverage += m_table[row][offset + inWindow + halfWindowSize];
        }
        leftAverage  /= halfWindowSize;
        rightAverage /= halfWindowSize;
        leftAverages[row]  = leftAverage;
        rightAverages[row] = rightAverage;
    }
    return distanceFunction->distance(leftAverages, rightAverages);
}


Table* Table::transpose() const {
    Table* result = new Table(m_width, m_height);
    for (size_t col = 0; col < m_width; ++col)
    {
        for (size_t row = 0; row < m_height; ++row)
        {
            result->set(col, row, at(row, col) );
        }
    }

    return result;
}

void Table::normalizeWithStat(const vector<ResultStat>& stat)
{
    uint h = height();
    uint s = stat.size();
    for (uint spotId = 0; spotId < h; spotId++) {
        for (uint j = 0; j < s; j++) {
            uint y = std::min(spotId+j, h-1);

            E value = m_table[y][j];
            if (value != 0.0) {
                m_table[y][j] = (value - stat[j].mean) / stat[j].deviation;
            }
        }
    }
}

void Table::replaceColumns(uint first, uint second)
{
    for (uint row = 0; row < m_height; ++row)
    {
        std::swap(m_table[row][first], m_table[row][second]);
    }
}
