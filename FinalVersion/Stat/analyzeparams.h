#ifndef ANALYZEPARAMS_H
#define ANALYZEPARAMS_H

// Parameters for the calculations.
// Comes from the GUI/command line
class AnalyzeParams
{
public:
    enum Distance {
        EUCLIDIAN_SQUARE,
        EUCLIDIAN,
        MANHATTAN,
        RENKONEN_INDEX_COMPLEMENTER,
        CZEKANOWSKI
    };

    enum Randomization {
        NONE,
        RANDOM_SHIFT,
        PLOT_RANDOMIZATION
    };

    AnalyzeParams() {}

    AnalyzeParams(
            Distance dist
            , Randomization randomization
            , int randomizationCount
            , int maxHalfWindow);

    Distance distance() const {return m_distance;}

    Randomization randomization() const {return m_randomization;}

    int randomizationCount() const {return m_randomizationCount;}

    int maxHalfWindow() const {return m_maxHalfWindow;}

private:
    Distance m_distance;
    Randomization m_randomization;
    int m_randomizationCount;
    int m_maxHalfWindow;
};

#endif // ANALYZEPARAMS_H
