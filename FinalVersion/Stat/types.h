// Contains common types used through the program

#ifndef TYPES_H
#define TYPES_H

#include <vector>

// The type of the elements in the table
typedef double E;
// Common vector tyoe
typedef std::vector<E> Vector;

typedef double Distance;

typedef unsigned int uint;

struct ResultStat
{
    double mean;
    double deviation;
    double p10;
    double p05;
    double p025;
    double p01;

    // Default constructor
    ResultStat()
        : mean(0.0)
        , deviation(0.0)
        , p10(0.0)
        , p05(0.0)
        , p025(0.0)
        , p01(0.0)
    {}
};

#endif // TYPES_H
