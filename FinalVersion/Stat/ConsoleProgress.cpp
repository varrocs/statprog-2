
#include <stdio.h>
#include "IProgress.h"

void ConsoleProgress::onProgressUpdate()
{
    printf("\r%.2f%%     ", percentage()*100.0);
}

