/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.0.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTableView>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionOpen;
    QAction *actionSave;
    QAction *actionExit;
    QWidget *centralWidget;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *leftLayout;
    QTableView *tableView;
    QProgressBar *progressBar;
    QVBoxLayout *rightLayout;
    QGroupBox *distanceGroupBox;
    QVBoxLayout *verticalLayout;
    QRadioButton *DIST_1_EUCL;
    QRadioButton *DIST_2_SQ_EUCL;
    QRadioButton *DIST_3_MANH;
    QRadioButton *DIST_4_RENKONEN;
    QRadioButton *DIST_5_DZ;
    QGroupBox *randomizationBox;
    QVBoxLayout *verticalLayout_2;
    QRadioButton *RAND_1_SHIFT;
    QRadioButton *RAND_2_PLOT;
    QGroupBox *resultGroupBox;
    QVBoxLayout *verticalLayout_3;
    QRadioButton *RESULT_3_NORMALIZED;
    QRadioButton *RESULT_2_DISTANCES;
    QRadioButton *RESULT_1_STATISTICS;
    QRadioButton *RESULT_4_ORIGINAL;
    QFormLayout *formLayout;
    QLabel *label;
    QSpinBox *SPIN_HALF_WIN;
    QLabel *label_2;
    QSpinBox *SPIN_RANDOMIZATIONS;
    QSpacerItem *verticalSpacer;
    QPushButton *analyzeButton;
    QPushButton *abortButton;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(964, 627);
        actionOpen = new QAction(MainWindow);
        actionOpen->setObjectName(QStringLiteral("actionOpen"));
        actionSave = new QAction(MainWindow);
        actionSave->setObjectName(QStringLiteral("actionSave"));
        actionExit = new QAction(MainWindow);
        actionExit->setObjectName(QStringLiteral("actionExit"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        horizontalLayout = new QHBoxLayout(centralWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        leftLayout = new QVBoxLayout();
        leftLayout->setSpacing(6);
        leftLayout->setObjectName(QStringLiteral("leftLayout"));
        tableView = new QTableView(centralWidget);
        tableView->setObjectName(QStringLiteral("tableView"));

        leftLayout->addWidget(tableView);

        progressBar = new QProgressBar(centralWidget);
        progressBar->setObjectName(QStringLiteral("progressBar"));
        progressBar->setValue(0);

        leftLayout->addWidget(progressBar);


        horizontalLayout->addLayout(leftLayout);

        rightLayout = new QVBoxLayout();
        rightLayout->setSpacing(6);
        rightLayout->setObjectName(QStringLiteral("rightLayout"));
        distanceGroupBox = new QGroupBox(centralWidget);
        distanceGroupBox->setObjectName(QStringLiteral("distanceGroupBox"));
        distanceGroupBox->setFlat(false);
        distanceGroupBox->setCheckable(false);
        verticalLayout = new QVBoxLayout(distanceGroupBox);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        DIST_1_EUCL = new QRadioButton(distanceGroupBox);
        DIST_1_EUCL->setObjectName(QStringLiteral("DIST_1_EUCL"));
        DIST_1_EUCL->setChecked(true);

        verticalLayout->addWidget(DIST_1_EUCL);

        DIST_2_SQ_EUCL = new QRadioButton(distanceGroupBox);
        DIST_2_SQ_EUCL->setObjectName(QStringLiteral("DIST_2_SQ_EUCL"));

        verticalLayout->addWidget(DIST_2_SQ_EUCL);

        DIST_3_MANH = new QRadioButton(distanceGroupBox);
        DIST_3_MANH->setObjectName(QStringLiteral("DIST_3_MANH"));

        verticalLayout->addWidget(DIST_3_MANH);

        DIST_4_RENKONEN = new QRadioButton(distanceGroupBox);
        DIST_4_RENKONEN->setObjectName(QStringLiteral("DIST_4_RENKONEN"));
        DIST_4_RENKONEN->setEnabled(true);

        verticalLayout->addWidget(DIST_4_RENKONEN);

        DIST_5_DZ = new QRadioButton(distanceGroupBox);
        DIST_5_DZ->setObjectName(QStringLiteral("DIST_5_DZ"));

        verticalLayout->addWidget(DIST_5_DZ);


        rightLayout->addWidget(distanceGroupBox);

        randomizationBox = new QGroupBox(centralWidget);
        randomizationBox->setObjectName(QStringLiteral("randomizationBox"));
        verticalLayout_2 = new QVBoxLayout(randomizationBox);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        RAND_1_SHIFT = new QRadioButton(randomizationBox);
        RAND_1_SHIFT->setObjectName(QStringLiteral("RAND_1_SHIFT"));
        RAND_1_SHIFT->setChecked(true);

        verticalLayout_2->addWidget(RAND_1_SHIFT);

        RAND_2_PLOT = new QRadioButton(randomizationBox);
        RAND_2_PLOT->setObjectName(QStringLiteral("RAND_2_PLOT"));

        verticalLayout_2->addWidget(RAND_2_PLOT);


        rightLayout->addWidget(randomizationBox);

        resultGroupBox = new QGroupBox(centralWidget);
        resultGroupBox->setObjectName(QStringLiteral("resultGroupBox"));
        verticalLayout_3 = new QVBoxLayout(resultGroupBox);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        RESULT_3_NORMALIZED = new QRadioButton(resultGroupBox);
        RESULT_3_NORMALIZED->setObjectName(QStringLiteral("RESULT_3_NORMALIZED"));

        verticalLayout_3->addWidget(RESULT_3_NORMALIZED);

        RESULT_2_DISTANCES = new QRadioButton(resultGroupBox);
        RESULT_2_DISTANCES->setObjectName(QStringLiteral("RESULT_2_DISTANCES"));

        verticalLayout_3->addWidget(RESULT_2_DISTANCES);

        RESULT_1_STATISTICS = new QRadioButton(resultGroupBox);
        RESULT_1_STATISTICS->setObjectName(QStringLiteral("RESULT_1_STATISTICS"));

        verticalLayout_3->addWidget(RESULT_1_STATISTICS);

        RESULT_4_ORIGINAL = new QRadioButton(resultGroupBox);
        RESULT_4_ORIGINAL->setObjectName(QStringLiteral("RESULT_4_ORIGINAL"));

        verticalLayout_3->addWidget(RESULT_4_ORIGINAL);


        rightLayout->addWidget(resultGroupBox);

        formLayout = new QFormLayout();
        formLayout->setSpacing(6);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label);

        SPIN_HALF_WIN = new QSpinBox(centralWidget);
        SPIN_HALF_WIN->setObjectName(QStringLiteral("SPIN_HALF_WIN"));
        SPIN_HALF_WIN->setMinimum(1);
        SPIN_HALF_WIN->setMaximum(100000);
        SPIN_HALF_WIN->setValue(25);

        formLayout->setWidget(1, QFormLayout::FieldRole, SPIN_HALF_WIN);

        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));

        formLayout->setWidget(3, QFormLayout::LabelRole, label_2);

        SPIN_RANDOMIZATIONS = new QSpinBox(centralWidget);
        SPIN_RANDOMIZATIONS->setObjectName(QStringLiteral("SPIN_RANDOMIZATIONS"));
        SPIN_RANDOMIZATIONS->setMinimum(1);
        SPIN_RANDOMIZATIONS->setMaximum(100000000);
        SPIN_RANDOMIZATIONS->setSingleStep(10);
        SPIN_RANDOMIZATIONS->setValue(1000);

        formLayout->setWidget(3, QFormLayout::FieldRole, SPIN_RANDOMIZATIONS);


        rightLayout->addLayout(formLayout);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        rightLayout->addItem(verticalSpacer);

        analyzeButton = new QPushButton(centralWidget);
        analyzeButton->setObjectName(QStringLiteral("analyzeButton"));
        analyzeButton->setEnabled(false);

        rightLayout->addWidget(analyzeButton);

        abortButton = new QPushButton(centralWidget);
        abortButton->setObjectName(QStringLiteral("abortButton"));
        abortButton->setEnabled(false);

        rightLayout->addWidget(abortButton);


        horizontalLayout->addLayout(rightLayout);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 964, 25));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QStringLiteral("menuFile"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuFile->menuAction());
        menuFile->addAction(actionOpen);
        menuFile->addAction(actionSave);
        menuFile->addAction(actionExit);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0));
        actionOpen->setText(QApplication::translate("MainWindow", "Open", 0));
        actionOpen->setShortcut(QApplication::translate("MainWindow", "Ctrl+O", 0));
        actionSave->setText(QApplication::translate("MainWindow", "Save", 0));
        actionSave->setShortcut(QApplication::translate("MainWindow", "Ctrl+S", 0));
        actionExit->setText(QApplication::translate("MainWindow", "Exit", 0));
        actionExit->setShortcut(QApplication::translate("MainWindow", "Ctrl+Q", 0));
        distanceGroupBox->setTitle(QApplication::translate("MainWindow", "Distance function", 0));
        DIST_1_EUCL->setText(QApplication::translate("MainWindow", "Eucdlidian", 0));
        DIST_2_SQ_EUCL->setText(QApplication::translate("MainWindow", "Squared Euclidian Distance", 0));
        DIST_3_MANH->setText(QApplication::translate("MainWindow", "Manhattan", 0));
        DIST_4_RENKONEN->setText(QApplication::translate("MainWindow", "Renkonen", 0));
        DIST_5_DZ->setText(QApplication::translate("MainWindow", "Czekanowski", 0));
        randomizationBox->setTitle(QApplication::translate("MainWindow", "Randomization", 0));
        RAND_1_SHIFT->setText(QApplication::translate("MainWindow", "Random shift", 0));
        RAND_2_PLOT->setText(QApplication::translate("MainWindow", "Plot randomization", 0));
        resultGroupBox->setTitle(QApplication::translate("MainWindow", "Results", 0));
        RESULT_3_NORMALIZED->setText(QApplication::translate("MainWindow", "Normalized", 0));
        RESULT_2_DISTANCES->setText(QApplication::translate("MainWindow", "Distances", 0));
        RESULT_1_STATISTICS->setText(QApplication::translate("MainWindow", "Statistics", 0));
        RESULT_4_ORIGINAL->setText(QApplication::translate("MainWindow", "Original", 0));
        label->setText(QApplication::translate("MainWindow", "Maximum half window size", 0));
        label_2->setText(QApplication::translate("MainWindow", "Number of randomizations", 0));
        analyzeButton->setText(QApplication::translate("MainWindow", "Analyze", 0));
        abortButton->setText(QApplication::translate("MainWindow", "Abort", 0));
        menuFile->setTitle(QApplication::translate("MainWindow", "File", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
