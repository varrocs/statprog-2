#ifndef RANDOMIZERFUNCTION_H
#define RANDOMIZERFUNCTION_H

#include <string>
class Table;

// Sets the random seed, on windows it has to be called for all threads
void randomize(uint* baseSeed = nullptr);
unsigned int random(unsigned int N);

// --------------------   Interface   --------------------

class RandomizerFunction
{
public:
    // Does the randomization
    virtual Table* randomize(const Table& table) const = 0;
    // Returns the name of the function
    virtual std::string toString() const = 0;
    virtual ~RandomizerFunction(){}
};

// -------------------- Implementations --------------------


class PlotRandomize : public RandomizerFunction
{
public:
    virtual Table* randomize(const Table& table) const;
    virtual std::string toString() const;
};

class RandomShift : public RandomizerFunction
{
public:
    virtual Table* randomize(const Table& table) const;
    virtual std::string toString() const;
};

class NoneRandomize: public RandomizerFunction
{
public:
    virtual Table* randomize(const Table& table) const;
    virtual std::string toString() const;
};

#endif // RANDOMIZERFUNCTION_H
