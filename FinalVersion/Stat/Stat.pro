#-------------------------------------------------
#
# Project created by QtCreator 2013-11-25T11:03:30
#
#-------------------------------------------------

CONFIG      += qt
QT          += core gui widgets

TARGET = Stat
TEMPLATE = app


QMAKE_CXXFLAGS += -std=c++11

# For windows static compilation
win32 {
QMAKE_LFLAGS += -static -static-libgcc -static-libstdc++
}

#Alternative compiler (better error messages)
#QMAKE_CXX = clang++
# No GUI, just command line, incomplete
#DEFINES += CLI
# Just one thread, no parallel execution
#DEFINES += ONE_THREADED


TARGET = Stat
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp\
        table.cpp\
        logic.cpp\
        analyzeparams.cpp\
        randomizerfunction.cpp\
        distancefunction.cpp\
        ConsoleProgress.cpp

HEADERS  +=  table.h\
        mainwindow.h\
        ui_mainwindow.h\
        logic.h\
        analyzeparams.h\
        randomizerfunction.h\
        distancefunction.h \
        types.h \
        IProgress.h

FORMS    += mainwindow.ui
