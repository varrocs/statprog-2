#include "analyzeparams.h"

AnalyzeParams::AnalyzeParams(Distance dist
                             , Randomization randomization
                             , int randomizationCount
                             , int maxHalfWindow)
    : m_distance(dist)
    , m_randomization(randomization)
    , m_randomizationCount(randomizationCount)
    , m_maxHalfWindow(maxHalfWindow)
{
}
